#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/xphoto.hpp"


#include "std_srvs/SetBool.h"
#include <vector>

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>
#include <eigen3/Eigen/LU>

#include <tf2_ros/transform_listener.h>
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include <geometry_msgs/TransformStamped.h>


    template <class T>
    /**
        * \fn bool getRosParam
        * \brief Load a rosparam to a variable
        * \param const std::string& param_name the name of the ros param
        * \param T& param_data the variable linked to the ros param
        * \return true if the ros param exist and can be linked to the variable
        */
    bool getRosParam(const std::string& param_name, T& param_data, bool verbose = false)
    {
        if (!ros::param::get(param_name, param_data))
        {
            ROS_FATAL_STREAM(" Problem loading parameters " << param_name << ". Check the Yaml config file. Killing ros");
            ros::shutdown();
        }
        else
        {
        if (verbose)
            ROS_INFO_STREAM(param_name << " : " << param_data);
        }
        return true;
    };

    /**
        * \fn bool getRosParam
        * \brief Load a rosparam to a variable
        * \param const std::string& param_name the name of the ros param
        * \param Eigen::VectorXd& param_data a Eigen vector linked to the std::vector ros param
        * \return true if the ros param exist and can be linked to the variable
        */
    bool getRosParam(const std::string& param_name, Eigen::VectorXd& param_data, bool verbose = false)
    {
        std::vector<double> std_param;
        if (!ros::param::get(param_name, std_param))
        {
            ROS_FATAL_STREAM(" Problem loading parameters " << param_name << ". Check the Yaml config file. Killing ros");
            ros::shutdown();
        }
        else
        {
        if (std_param.size() == param_data.size())
        {
            for (int i = 0; i < param_data.size(); ++i)
            param_data(i) = std_param[i];
            if (verbose)
            ROS_INFO_STREAM(param_name << " : " << param_data.transpose());
            return true;
        }
        else
        {
            ROS_FATAL_STREAM("Wrong matrix size for param " << param_name << ". Check the Yaml config file. Killing ros");
            ros::shutdown();
        }
        }
        return(true);
    };


class ImageConverter
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_;
  cv_bridge::CvImagePtr cv_ptr;
  
  std::string algorithm;


public:
  ImageConverter(ros::NodeHandle& nh)
    : 
    nh_(nh),
    it_(nh)
  {

        std::string nodeName = ros::this_node::getName();
        nodeName += "/";    
    std::string cameraTopic;
    std::string cameraSub;
        getRosParam(nodeName+"camera_sub",cameraSub);

    getRosParam(nodeName+"camera_topic",cameraTopic);
    
    cameraTopic = cameraSub + "/" + cameraTopic;

    image_sub_ = it_.subscribe(cameraTopic, 1000,
        &ImageConverter::imageCb,this);
    image_pub_ = it_.advertise(cameraTopic+"_flip", 1000);

    ros::spin();
  }

  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {

    try
    {
        cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
        cv::Mat src =  cv_ptr->image;
        cv::Mat res;

        cv::flip(src,res,1);
        
        cv_ptr->image= res;
        image_pub_.publish(cv_ptr->toImageMsg());

    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());

      return;
    }
  }

    
};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "flip_cam_node");
  ros::NodeHandle nh("~");
  ImageConverter ic(nh);
  return 0;
}
