#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import cv2
import numpy as np
import rospy

from sensor_msgs.msg import CameraInfo
from msg_srv_action_gestion.srv import SetString

"""
Goal of this node : to optimize the output of a given camera.

For now : 
    * autofocus at a position in camera frame

Maybe : 
    * automatic white balance?

"""