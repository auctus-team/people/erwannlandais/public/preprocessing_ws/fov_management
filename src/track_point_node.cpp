#include "fov_management/track_point_node.h"

/* RECALLS : 
  1920x1080 : 
    1920 : width
    1080 : height
https://docs.opencv.org/3.4/d9/d0c/group__calib3d.html#gafa5aa3f40a472bda956b4c27bec67438

image : M[0:height,0:width] ==> confirmed
image.size() : [1920 x 1080] : [width x height]
x = M[:,i] (in 0:width = 1920)
y = M[i,:] (in 0:height = 1080)
==> cx : on col : width
==> cy : on row : height

==> u : on col
==> v : on row

Move on x axis ==> stay at same row, move on cols
Move on y axis ==> stay at same col, move on rows

*/

namespace camToView
{

    trackPoint::trackPoint(ros::NodeHandle & nh):
    nh_(nh),
    tU(nh,true),
    it_(nh)
    {   
        
        gotCamInfo = false;
        initialized = false;
        picTaken = false;
        picTreated = false;

        std::string nodeName = ros::this_node::getName();
        nodeName += "/";
        std::string controller_name;
        std::string input_type;

        // Camera namespace
        rosWrap::getRosParam(nodeName+"camera_sub",cameraSub);
        // Name of the input topic in the camera namepace
        // Output topic name will be <inputCameraTopic>_infos
        rosWrap::getRosParam(nodeName+"camera_topic",inputCameraTopic);
        // Referential of the point tracked
        rosWrap::getRosParam(nodeName+"tracked_ref",trackedRef);
        // Global base referential (ex : world)
        //rosWrap::getRosParam(nodeName+"global_ref", globalRef);
        // Name of the camera referential (TF)
        rosWrap::getRosParam(nodeName+"camera_ref", cameraRef);
        // Identifiant of camera (should be : /dev/video<camID>)
        rosWrap::getRosParam(nodeName+"camID", camID);

        double node_rate = 100;
        rosWrap::getRosParam(nodeName+"node_rate", node_rate,1);

        ros::Rate rate(node_rate);


        inputCameraTopic = cameraSub + "/" + inputCameraTopic;
        outputCameraTopic = inputCameraTopic + "_infos";

        typeInput = camToView::inputType::ROS;

        // rosWrap::getRosParam(nodeName+"input_type", input_type);

        // if (input_type == "GST")
        // {
        //     typeInput = camToView::inputType::GST;
        // }

        // else if (input_type == "ROS")
        // {
        //     typeInput = camToView::inputType::ROS;
        // }

        std::string camera_info_topic = cameraSub+"/camera_info";

        ros::Subscriber intrinsic = nh.subscribe(camera_info_topic, 1, &trackPoint::cameraInfoSub, this);

        while (ros::ok())
        {
            if (gotCamInfo)
            {
                if (!initialized)
                {

                    initialize();
                    if (initialized)
                    {
                        intrinsic.shutdown();
                        ROS_INFO("FOV_MANAGER INITIALIZED");
                        if (typeInput == camToView::inputType::ROS)
                        {
                            image_sub_ = it_.subscribe(inputCameraTopic, 1000,
                            &trackPoint::imageCb, this);
                        }

                        image_pub_ = it_.advertise(outputCameraTopic, 1000);
                    }

                }
                else
                {
                    addInfosOnView();
                }

            }

            rate.sleep();
            ros::spinOnce();
        }



        // https://www.tutorialkart.com/opencv/python/opencv-python-resize-image/

        // https://github.com/yukkysaito/autoware_thesis/blob/master/ros/src/sensing/fusion/packages/points2image/nodes/points2image/points2image.cpp


    }



    void trackPoint::cameraInfoSub(const sensor_msgs::CameraInfo & msg)
    {
        if (!gotCamInfo)
        {
            pU.getInfosFromCI(msg);
            std::cout << "got cam info! "<< std::endl;
            gotCamInfo = true;
        }
    }

    int trackPoint::computeFocusValue(Eigen::Vector3d pos)
    {
        //std::cout << "pos : " << pos[2] << std::endl;
        float value = -1;
        float depth = pos[2];
        int i = 0;

        while (i < focusDistance.size() && value == -1)
        {
            if (depth < focusDistance[i])
            {
                value = focusValue[i];
            }
            else
            {
                i+=1;
            }
        }

        return(value);                                
    }


    void trackPoint::setFocusValue(Eigen::Vector3d pos)
    {
        if ( focusCorrection)
        {
            float val = computeFocusValue(pos);
            msg_srv_action_gestion::SetStringWR srv;

            std::string str_com = std::to_string(val);

            str_com = "focus_absolute=" + str_com;
            //"focus_automatic_continuous:0,focus_absolute:" + str_com;

            std::vector<std::string> commands = {
                "focus_automatic_continuous=0",
                str_com
            };

            for (int i = 0; i < commands.size(); i++)
            {

              std::string command = "v4l2-ctl -d /dev/video" + camID + " -c " + commands[i];
              char buffer[128];
              std::string result = "";    
              FILE* pipe = popen(command.c_str(), "r");

              // read till end of process:
              while (!feof(pipe)) {

                  // use buffer to read and add to result
                  if (fgets(buffer, 128, pipe) != NULL)
                    result += buffer;
              }

              if (result != "")
              {

                ROS_WARN("( %s ) : %s", commands[i].c_str(), result.c_str());
              }

            }

        }
        else
        {
            //ROS_INFO("camParams not found.");
        }
    }



  void trackPoint::addInfosOnView()
  {
    //
    // 
        if (!picTaken && allCvImg.size() > 0)
        {
            geometry_msgs::Pose camRef2TrackRef;            
            bool worked = tU.getPose(cameraRef,trackedRef,camRef2TrackRef);
                
            if (worked)
            {
                // reset focus value (at the depth of a tracked frame)
                Eigen::Vector3d fp(camRef2TrackRef.position.x,
                                    camRef2TrackRef.position.y,
                                camRef2TrackRef.position.z);
                setFocusValue(fp);  


                // now, take last picture

                picTreated = true;

                // here is your last picture! :)
                cv_bridge::CvImage cvImg(allCvImg[0]->header, allCvImg[0]->encoding, allCvImg[0]->image);
                allCvImg.erase(allCvImg.begin());
                cvImg.header.frame_id = cameraRef;
                picTreated = false;

                // do operations on picture

                // do 3D -> 2D transform
                std::vector<cv::Point3d> all3D;
                cv::Point3d pt3D(fp[0], fp[1], fp[2]);
                all3D.push_back(pt3D);

                std::vector<cv::Point2d> all2D;

                pU.from3Dto2D(all3D,all2D);

                cv::Point2d pt2D = all2D[0];

                //  --- Put whatever value you want on picture
                // (ex : here, put position of tracked point according
                // to camera referential)
                
                char* x;
                asprintf(&x, "(%3.3f, %3.3f, %3.3f)", 
                        fp[0], fp[1], fp[2] );
                std::string s_CI = x;

                // Ex : white background, green text
                pU.setTextOnPicture(cvImg.image,s_CI);

                // Put a green circle on the position of the
                // tracked point
                cv::circle(cvImg.image,
                        pt2D, // position on picture : cv::Point2d(x,y)
                        cvImg.image.size().height/64, // size of circle
                        cv::Scalar( 0, 255,0), // color
                        cv::FILLED,
                        cv::LINE_8 );

                cvImg.header.stamp = ros::Time::now();
                image_pub_.publish(cvImg.toImageMsg());

            }
        }
  }



    void trackPoint::initialize()
    {
        if (!initialized)
        {
            switch (typeInput)
            {
                case (camToView::inputType::GST):
                {
                    initialized = (camCommand != "");
                }break;

                case (camToView::inputType::ROS):
                {
                    initialized = true;
                }break;                
            }
        }
    }

    void trackPoint::imageCb(const sensor_msgs::ImageConstPtr& msg)
    {
        if (!picTreated)
        {
            picTaken = true;
            try
            {
                allCvImg.push_back( cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8) );
            }
            catch (cv_bridge::Exception& e)
            {
                ;;
            }    
            picTaken = false;
        }

    }

};



/**
 * This tutorial demonstrates simple sending of messages over the ROS system.
 */
int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "fov_manager");
  ros::NodeHandle nh;

  camToView::trackPoint fovM(nh);
  return 0;
}