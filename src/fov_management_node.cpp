#include "fov_management/fov_management_node.h"


/* RECALLS : 
  1920x1080 : 
    1920 : width
    1080 : height
https://docs.opencv.org/3.4/d9/d0c/group__calib3d.html#gafa5aa3f40a472bda956b4c27bec67438

image : M[0:height,0:width] ==> confirmed
image.size() : [1920 x 1080] : [width x height]
x = M[:,i] (in 0:width = 1920)
y = M[i,:] (in 0:height = 1080)
==> cx : on col : width
==> cy : on row : height

==> u : on col
==> v : on row

Move on x axis ==> stay at same row, move on cols
Move on y axis ==> stay at same col, move on rows

*/

namespace fovManagement
{



    /**
     * NB : pos must be expressed in camera referential.
    */
    float fovManager::computeFocusValue(Eigen::Vector3d pos)
    {
        //std::cout << "pos : " << pos[2] << std::endl;
        float value;
        float depth = pos[2];
        if (depth < 0.07)
        {
            value = 115;
        }
        else if (depth < 0.08)
        {
            value = 110;
        }
        else if (depth < 0.09)
        {
            value = 105;
        }
        else if (depth < 0.10)
        {
            value = 90;
        }                        
        else if (depth < 0.11)
        {
            value = 85.0;
        }

        else if (depth < 0.12)
        {
            value = 80.0;
        }

        else if (depth < 0.13)
        {
            value = 75.0;
        }

        else if (depth < 0.18)
        {
            value = 70.0;
        }


        else if (depth < 0.21)
        {
            value = 65.0;
        }
        else if (depth < 0.29)
        {
            value = 60.0;
        }
        else if (depth < 0.45)
        {
            value = 55.0;
        }
        else if (depth < 0.60)
        {
            value = 50.0;
        }
        else
        {
            value = 45.0;
        }   

        //std::cout << "value : " << value << std::endl;

        return(value);                                
    }

    bool fovManager::stopTrig(msg_srv_action_gestion::GetFloat::Request &req,
    msg_srv_action_gestion::GetFloat::Response &res)
    {
        double curTime = ros::Time::now().toSec();
        std::vector<double> result = pU.stopTimer(curTime);
        res.data.data = (result[1]-result[0]);
        res.success = true;
        gotStopTrig = true;
        return(true);
    }

    bool fovManager::pauseContinueTrig(std_srvs::SetBool::Request &req,
    std_srvs::SetBool::Response &res)
    {

        pU.pauseTimer(req.data, ros::Time::now().toSec());

        res.success = true;

        return(true);
    }



    void fovManager::manageTimerTrig(const std_msgs::Float64 & msg)
    {
        std::cout << "Got timer trigger!" << std::endl;  
        if (msg.data >= 0) 
        {
            pU.setTimer(msg.data);
        }
        double curTime = ros::Time::now().toSec();
        std::cout << "Start time : " << curTime << std::endl;
        pU.activateTimer(curTime);
        calledAfterTimerStop = false;
        gotStopTrig = false;
    }

    void fovManager::setFocusValue(Eigen::Vector3d pos)
    {
        if (camParams.exists() && focusCorrection)
        {
            float val = computeFocusValue(pos);
            msg_srv_action_gestion::SetStringWR srv;

            std::string str_com = std::to_string(val);

            str_com = "focus_automatic_continuous:0,focus_absolute:" + str_com;

            srv.request.data = str_com;

            int i = 0;
            bool send = false;
            while (i < 10 and !send)
            {
                if (camParams.call(srv))
                {
                    send = true;

                }
                i++;
            }
            if (!send)
            {
                ROS_INFO("Infos not sent to camParam");
            }

        }
        else
        {
            //ROS_INFO("camParams not found.");
        }
    }

    bool fovManager::ask_cam_info(std_srvs::SetBool::Request &req,
    std_srvs::SetBool::Response &res)
    {
        isCamInfoAsked = req.data;

        res.success = true;
        res.message = "ask_cam_info ok";

        ROS_INFO("GOT CAM INFO COMMAND");

        return(true);

    }


    bool fovManager::focusCalibTrigger(std_srvs::SetBool::Request &req,
    std_srvs::SetBool::Response &res)
    {
        focusCorrection = req.data;

        res.success = true;
        res.message = "focus_calib_trigger ok";


        return(true);

    }    

   bool fovManager::cameraStringServer(msg_srv_action_gestion::SetStringWR::Request &req,
               msg_srv_action_gestion::SetStringWR::Response &res)
    {
        res.success = true;
        res.result = "";
        if (req.data.size() > 0)
        {
            std::string dataMsg = req.data;
            std::vector<std::string> list = splitString(dataMsg,"#");
            res.success = false;
            if (list.size() > 1 && list.size()%2 == 0)
            {
                for (int i =0; i<list.size(); i+=2)
                {
                    if (list[i] == "CAMCOM")
                    {
                        if (!picTreated)
                        {
                            allCvImg.clear();
                            camCommand = list[i+1];
                            cap.reset(new cv::VideoCapture);
                            cap->open(camCommand,cv::CAP_GSTREAMER);

                            if (cap->isOpened())
                            {
                                res.success = true;
                                initialized = false;
                            }
                        }
                        else
                        {
                            res.success = false;
                        }
                    }
                    else if (list[i] == "CAMGET")
                    {
                        
                        // call to get new picture
                        if (cap->isOpened() and !picTreated)
                        {
                            //ROS_INFO("got new pic!");
                            picTaken = true;
                            std_msgs::Header hdr;
                            cv::Mat frame;
                            cap->read(frame);

                            hdr.stamp = ros::Time::now();
                            cv_bridge::CvImagePtr CIP = boost::shared_ptr<cv_bridge::CvImage>(new cv_bridge::CvImage(hdr,sensor_msgs::image_encodings::BGR8,frame));
                            allCvImg.push_back( CIP );

                            if (pubImg)
                            {
                                if (cntPubImg == fps_pub)
                                {
                                    canPubImg = true;
                                    cntPubImg = 0;
                                }
                                else
                                {
                                    cntPubImg+=1;
                                }
                            }

                            res.success = true;


                            picTaken = false;

                        }
                    }
                    else if (list[i] == "PING")
                    {
                        res.result = camCommand;
                        res.success = true;
                    }
                }
            }
        }
        return(true);
    }


    std::vector<std::string> fovManager::splitString(
        std::string str,
        std::string delimeter)
    {
        std::vector<std::string> splittedStrings = {};
        size_t pos = 0;

        while ((pos = str.find(delimeter)) != std::string::npos)
        {
            std::string token = str.substr(0, pos);
            if (token.length() > 0)
                splittedStrings.push_back(token);
            str.erase(0, pos + delimeter.length());
        }

        if (str.length() > 0)
            splittedStrings.push_back(str);
        return splittedStrings;
    }


    void fovManager::send_cam_info()
    {
        if (camInfoSender.exists() && camInfoSender2.exists())
        {        
            //ROS_INFO("SEND CAM INFO AS ASKED");
            msg_srv_action_gestion::SetCamInfo srv_ori;            
            msg_srv_action_gestion::SetCamInfo srv;

            srv.request.camInfo.height = height;
            srv.request.camInfo.width = width;
            //std::cout << camMat << std::endl;
            //ROS_INFO("OK1");
            for (int row=0; row<3; row++) {
                for (int col=0; col<4; col++) {
                    srv.request.camInfo.P[row*4+col] =  camMat.at<float>(row, col);
                    // std::cout <<  camMat.at<double>(row, col) << std::endl;
                    // std::cout << camMat.at<float>(row, col) << std::endl;
                    //std::cout << srv.request.camInfo.P[row*4+col] << std::endl;
                }
            }

            //std::cout << srv.request.camInfo.P << std::endl;
            //ROS_INFO("OK2");
            // std::cout << distCoeffs.size() << std::endl;
            // std::cout << distCoeffs.cols << std::endl;
            //                 std::cout << distCoeffs.rows << std::endl;
            for (int i = 0; i < distCoeffs.rows; i++)
            {
                srv.request.camInfo.D.push_back(  distCoeffs.at<float>(i,0) );
            }

            //ROS_INFO("OK3");

            int i = 0;
            while (i < 10)
            {
                bool send = false;
                if (camInfoSender.call(srv))
                {
                    ROS_INFO("SEND TO PVCT OK");
                    send = true;
                    srv.response = srv_ori.response;
                }
                else
                {
                    ROS_INFO("FAILED TO SEND cam_info TO PVCT");                    
                }
                if (send && camInfoSender2.call(srv))
                {
                    ROS_INFO("SEND TO MPM OK");
                    isCamInfoAsked = false;
                    i = 10;
                }
                else if (send == true)
                {
                    ROS_INFO("FAILED TO SEND cam_info TO MPM");                    
                }                
                i++;
            }
        }
    }

    void fovManager::cameraInfoSub(const sensor_msgs::CameraInfo & msg)
    {
        //std::cout << "got message!" << std::endl;
        if (!gotCamInfo)
        {


            height = msg.height;
            width = msg.width;
            // D --> distorsion params
            // K --> Intrinsic camera matrix for the raw (distorted) images.
            // P --> Projection/camera matrix

            cv::Mat1f dC(msg.D.size(),1); 
            cv::Mat1f cM(3,4);

            for (int i = 0; i < msg.D.size();i++)
            {
                dC(i) = msg.D[i];
            }

            for (int row=0; row<3; row++) {
                for (int col=0; col<4; col++) {
                    cM(row, col) = msg.P[row * 4 + col];
                }
            }
            

            cv::Mat1f K_temp(3,3);
            cv::Mat1f rvec_temp(3,3);
            cv::Mat1f Thomogeneous(4,1);

            cv::decomposeProjectionMatrix(cM, K_temp, rvec_temp, Thomogeneous);


            cv::Mat T_temp; // translation vector
            cv::convertPointsFromHomogeneous(Thomogeneous.reshape(4,1), T_temp);
            cv::Mat1f rvecR_temp(3,1);//rodrigues rotation matrix
            cv::Rodrigues(rvec_temp,rvecR_temp);

            // std::cout << "height : " << cM << std::endl;
            // std::cout << "width : " << cM << std::endl;
            // std::cout << "cM : " << cM << std::endl;
            // std::cout << "dC : " << dC << std::endl;
            // std::cout << "Rot_mat : " << rvec_temp << std::endl;
            // std::cout << "rVecR : " << rvecR_temp << std::endl;
            // std::cout << "T : " << T_temp << std::endl;
            // std::cout << "K : " << K_temp << std::endl;

            // T_temp.setTo(cv::Scalar(0.0));
            // rvecR_temp.setTo(cv::Scalar(0.0));

            // T_temp.at<float>(0,0) = 0.0;
            // T_temp.at<float>(1,0) = 0.0;
            // T_temp.at<float>(2,0) = 0.0;   
            // rvecR_temp.at<float>(0,0) = 0.0;
            // rvecR_temp.at<float>(1,0) = 0.0;
            // rvecR_temp.at<float>(2,0) = 0.0;                        


            // save all the matrix (in case we need them later)
            distCoeffs = dC;
            camMat = cM;
            rvecR = rvecR_temp;
            T = T_temp;
            K = K_temp;

            gotCamInfo = true;
            pU.getInfosFromCI(msg);
            std::cout << "got cam info! "<< std::endl;
        }
    }

    // Project 3D points in world to 2D points in picture 
    // From https://github.com/daviddoria/Examples/blob/master/c%2B%2B/OpenCV/ProjectPoints/ProjectPoints.cxx
    // https://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html#void%20projectPoints(InputArray%20objectPoints,%20InputArray%20rvec,%20InputArray%20tvec,%20InputArray%20cameraMatrix,%20InputArray%20distCoeffs,%20OutputArray%20imagePoints,%20OutputArray%20jacobian,%20double%20aspectRatio)
    void fovManager::from3Dto2D(std::vector<cv::Point3d> all3Dpoints, std::vector<cv::Point2d> & all2Dpoints)
    {
        if (gotCamInfo)
        {


            cv::projectPoints(all3Dpoints, rvecR, T, K, distCoeffs, all2Dpoints);
        }
    }

    void fovManager::from3Dto2D(std::vector<cv::Point3d> all3Dpoints, cv::Mat Ts, cv::Mat rvecRs, std::vector<cv::Point2d> & all2Dpoints)
    {
        if (gotCamInfo)
        {
            cv::projectPoints(all3Dpoints, rvecRs, Ts, K, distCoeffs, all2Dpoints);
        }
    }

    // From https://stackoverflow.com/questions/51272055/opencv-unproject-2d-points-to-3d-with-known-depth-z
    void fovManager::from2Dto3D(std::vector<cv::Point2d> all2Dpoints,
                                    std::vector<double>  allDepth,
                                    std::vector<cv::Point3d> & all3Dpoints) {
        // intrinsics : K
        // distorsion : distCoeffs
        float f_x = K.at<float>(0, 0);
        float f_y = K.at<float>(1, 1);
        float c_x = K.at<float>(0, 2);
        float c_y = K.at<float>(1, 2);

        //std::cout << f_x << " ; " << f_y << " ; " << c_x << " ; " << c_y << std::endl;


        // Step 1. Undistort
        std::vector<cv::Point2d> points_undistorted;

        if (!all2Dpoints.empty()) {
            cv::undistortPoints(all2Dpoints, points_undistorted, K,
                            distCoeffs, cv::noArray(), camMat);
            // cv::undistortPoints(all2Dpoints, points_undistorted, K,
            //                     distCoeffs);       
        }

        // for (int i =0; i < points_undistorted.size(); i++)
        // {
        //     std::cout << points_undistorted[i].x << " ; " << points_undistorted[i].y << std::endl;
        // }

        // Step 2. Reproject
        //std::vector<cv::Point3d> result;
        //all3DPoints.reserve(all2Dpoints.size());
        for (size_t idx = 0; idx < points_undistorted.size(); ++idx) {
            const double z = allDepth[idx];
            all3Dpoints.push_back(
                cv::Point3d(  ( (points_undistorted[idx].x - c_x) / f_x) * z,
                            ( ( points_undistorted[idx].y - c_y) / f_y )* z, z) );
        }

    }


    void fovManager::from2Dto3D(std::vector<cv::Point2d> all2Dpoints,
                                    std::vector<double>  allDepth,
                                    std::vector<cv::Point3d> & all3Dpoints,
                                    Eigen::Affine3d camInWorld)
    {

        from2Dto3D(all2Dpoints,allDepth,all3Dpoints);

        for (int i = 0; i < all3Dpoints.size(); i++)
        {
            Eigen::Vector3d pt_cam(all3Dpoints[i].x,all3Dpoints[i].y,all3Dpoints[i].z );
            Eigen::Vector3d pt_world = camInWorld * pt_cam;
            all3Dpoints[i].x = pt_world(0,0);
            all3Dpoints[i].y = pt_world(1,0);
            all3Dpoints[i].z = pt_world(2,0);                        
        }
        
    }

    void fovManager::imageCb(const sensor_msgs::ImageConstPtr& msg)
    {
        if (!picTreated)
        {
            picTaken = true;
            try
            {
                allCvImg.push_back( cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8) );
                //curPic = cv_ptr->image;
            }
            catch (cv_bridge::Exception& e)
            {
                ;;
            }    
            picTaken = false;
        }

    }



    void fovManager::getPlaneLimitsAtDepth_World(Eigen::Affine3d centerPoseInCam, Eigen::Affine3d camInWorld, 
    double depth, 
    cv::Point2d & oriPt,
    int & hPlane,
    int & wPlane)
    {


            Eigen::Vector3d botRight(halfWL,halfWL,halfWL);
            botRight[vDir_ind] = depth;

            Eigen::Vector3d topLeft(-halfWL,-halfWL,-halfWL);
            topLeft[vDir_ind] = depth;

            Eigen::Vector3d posBR(centerPoseInCam(0,3), centerPoseInCam(1,3), centerPoseInCam(2,3));
            posBR = posBR + botRight;

            Eigen::Vector3d posTL(centerPoseInCam(0,3), centerPoseInCam(1,3), centerPoseInCam(2,3));
            posTL = posTL + topLeft;

            centerPoseInCam(vDir_ind,3) = centerPoseInCam(vDir_ind,3)+ depth; 

            Eigen::Affine3d centerPoseInWorld = camInWorld * centerPoseInCam;

            //std::cout << "centerPoseInWorld : " << centerPoseInWorld.matrix() << std::endl;

            std::vector<cv::Point3d> lims3D;

            std::vector<double> allDepth;

            allDepth.push_back(posBR(vDir_ind));

            allDepth.push_back(posTL(vDir_ind));

            allDepth.push_back(centerPoseInCam(vDir_ind,3));


            posTL = camInWorld*posTL;
            posBR = camInWorld*posBR;


            lims3D.push_back(cv::Point3d(posBR(0), posBR(1), posBR(2)) );
            lims3D.push_back(cv::Point3d(posTL(0), posTL(1), posTL(2)) );
            lims3D.push_back(cv::Point3d(centerPoseInWorld(0,3), centerPoseInWorld(1,3), centerPoseInWorld(2,3)));

            // set T and Rvec according to camInWorld
            cv::Mat1f T_temp(3,1); // translation vector            
            T_temp.at<float>(0,0) = camInWorld(0,3);
            T_temp.at<float>(1,0) = camInWorld(1,3);
            T_temp.at<float>(2,0) = camInWorld(2,3);  
            cv::Mat1f rvec_temp(3,3);

            for (int row=0; row<3; row++) {
                for (int col=0; col<3; col++) {
                    rvec_temp(row, col) = camInWorld(row, col);
                }
            }
            cv::Mat1f rvecR_temp(3,1);
            cv::Rodrigues(rvec_temp,rvecR_temp);


            std::vector<cv::Point2d> lims2D;

            std::vector<cv::Point3d> backTo3D;

            from3Dto2D(lims3D,T_temp, rvecR_temp,lims2D);

            from2Dto3D(lims2D,allDepth,backTo3D,camInWorld);

            // for (int i =0; i < lims3D.size(); i++)
            // {
            //     std::cout << "3D : " << lims3D[i].x << " ; " << lims3D[i].y << " ; " << lims3D[i].z << std::endl;
            //     std::cout << "2D (row x col) : " << lims2D[i].y << " ; " << lims2D[i].x << std::endl;
            //     std::cout << "Back to 3D : " << backTo3D[i].x << " ; " << backTo3D[i].y << " ; " << backTo3D[i].z << std::endl;
            //     std::cout << "~~~~~~" << std::endl;

            // }
           

            int minH = std::min(lims2D[0].y, lims2D[1].y);
            int maxH = std::max(lims2D[0].y, lims2D[1].y);
            
            int minW = std::min(lims2D[0].x, lims2D[1].x);
            int maxW = std::max(lims2D[0].x, lims2D[1].x);

            hPlane = std::min( maxH-minH, height);
            wPlane = std::min(maxW-minW, width);

            oriPt.x = minW;
            oriPt.y = minH;

            //std::cout << "Poss 1 : " << oriPt.y << " ; " << oriPt.x << std::endl;

            oriPt.x = int(lims2D[2].x-wPlane/2);
            oriPt.y = int(lims2D[2].y-hPlane/2);


            //std::cout << "Poss 2 : " << oriPt.y << " ; " << oriPt.x << std::endl;

            // Most of the time (except at begin), Poss1 = Poss2

    }


    void fovManager::getPlaneLimitsAtDepth(Eigen::Affine3d centerPoseInCam,  
    double depth, 
    cv::Point2d & oriPt,
    int & hPlane,
    int & wPlane)
    {


            Eigen::Vector3d botRight(halfWL,halfWL,halfWL);
            botRight[vDir_ind] = depth;

            Eigen::Vector3d topLeft(-halfWL,-halfWL,-halfWL);
            topLeft[vDir_ind] = depth;

            Eigen::Vector3d posBR(centerPoseInCam(0,3), centerPoseInCam(1,3), centerPoseInCam(2,3));
            posBR = posBR + botRight;

            Eigen::Vector3d posTL(centerPoseInCam(0,3), centerPoseInCam(1,3), centerPoseInCam(2,3));
            posTL = posTL + topLeft;

            centerPoseInCam(vDir_ind,3) = centerPoseInCam(vDir_ind,3)+ depth; 

            std::vector<cv::Point3d> lims3D;

            std::vector<double> allDepth;

            allDepth.push_back(posBR(2));

            allDepth.push_back(posTL(2));

            allDepth.push_back(centerPoseInCam(2,3));



            lims3D.push_back(cv::Point3d(posBR(0), posBR(1), posBR(2)) );
            lims3D.push_back(cv::Point3d(posTL(0), posTL(1), posTL(2)) );
            lims3D.push_back(cv::Point3d(centerPoseInCam(0,3), centerPoseInCam(1,3), centerPoseInCam(2,3)));

            std::vector<cv::Point2d> lims2D;

            std::vector<cv::Point3d> backTo3D;

            from3Dto2D(lims3D,lims2D);

            from2Dto3D(lims2D,allDepth,backTo3D);

            // std::vector<std::string> pname = {"botRight","topLeft","center"};

            // for (int i =0; i < lims3D.size(); i++)
            // {
            //     std::cout << pname[i] << " : " << std::endl;
            //     std::cout << "3D : " << lims3D[i].x << " ; " << lims3D[i].y << " ; " << lims3D[i].z << std::endl;
            //     std::cout << "2D (row x col) : " << lims2D[i].y << " ; " << lims2D[i].x << std::endl;
            //     std::cout << "Back to 3D : " << backTo3D[i].x << " ; " << backTo3D[i].y << " ; " << backTo3D[i].z << std::endl;
            //     Eigen::Vector3d pt_cam(backTo3D[i].x,backTo3D[i].y,backTo3D[i].z );
            //     Eigen::Vector3d pt_world = camInWorld * pt_cam;
            //     std::cout << "Back to 3D (world) : " << pt_world<< std::endl;
            //     std::cout << "~~~~~~" << std::endl;
            // }
           

            int minH = std::min(lims2D[0].y, lims2D[1].y);
            int maxH = std::max(lims2D[0].y, lims2D[1].y);
            
            int minW = std::min(lims2D[0].x, lims2D[1].x); 
            int maxW = std::max(lims2D[0].x, lims2D[1].x);

            hPlane = std::min( maxH-minH, height);
            wPlane = std::min(maxW-minW, width);

            oriPt.x = minW;
            oriPt.y = minH;

            //std::cout << "Poss 1 : " << oriPt.y << " ; " << oriPt.x << std::endl;

            oriPt.x = int(lims2D[2].x-wPlane/2);
            oriPt.y = int(lims2D[2].y-hPlane/2);

            //std::cout << "Poss 2 : " << oriPt.y << " ; " << oriPt.x << std::endl;

            // Most of the time (except at begin), Poss1 = Poss2

    }

    // Returns, for a given tracked frame expressed in camera referential (trackedPoseInCam),
    // the 2D position of the top left limit on the image plane and the heights and width limits
    // of the frame around this point (determined by a fixed distance halfWL)
    void fovManager::getPlaneLimitsAtDepth_PU(Eigen::Affine3d trackedPoseInCam,  
    cv::Point2d & oriPt,
    int & hPlane,
    int & wPlane)
    {

            Eigen::Vector3d posBR(trackedPoseInCam(0,3)+halfWL, trackedPoseInCam(1,3)+halfWL, trackedPoseInCam(2,3));

            Eigen::Vector3d posTL(trackedPoseInCam(0,3)-halfWL, trackedPoseInCam(1,3)-halfWL, trackedPoseInCam(2,3));

            std::vector<cv::Point3d> lims3D;

            std::vector<double> allDepth;

            allDepth.push_back(posBR(2));

            allDepth.push_back(posTL(2));

            allDepth.push_back(trackedPoseInCam(2,3));



            lims3D.push_back(cv::Point3d(posBR(0), posBR(1), posBR(2)) );
            lims3D.push_back(cv::Point3d(posTL(0), posTL(1), posTL(2)) );
            lims3D.push_back(cv::Point3d(trackedPoseInCam(0,3), trackedPoseInCam(1,3), trackedPoseInCam(2,3)));

            std::vector<cv::Point2d> lims2D;

            std::vector<cv::Point3d> backTo3D;

            pU.from3Dto2D(lims3D,lims2D);

            int minH = std::min(lims2D[0].y, lims2D[1].y);
            int maxH = std::max(lims2D[0].y, lims2D[1].y);
            
            int minW = std::min(lims2D[0].x, lims2D[1].x); 
            int maxW = std::max(lims2D[0].x, lims2D[1].x);

            hPlane = std::min( maxH-minH, height);
            wPlane = std::min(maxW-minW, width);
            
            oriPt.x = lims2D[1].x;
            oriPt.y = lims2D[1].y;
            
            // oriPt.x = lims2D[1].x;
            // oriPt.y = lims2D[1].y;


            // oriPt.x = minW;
            // oriPt.y = minH;

            //std::cout << "Poss 1 : " << oriPt.y << " ; " << oriPt.x << std::endl;

            // oriPt.x = int(lims2D[2].x-wPlane/2);
            // oriPt.y = int(lims2D[2].y-hPlane/2);

            //std::cout << "Poss 2 : " << oriPt.y << " ; " << oriPt.x << std::endl;

            // Most of the time (except at begin), Poss1 = Poss2

    }

  void fovManager::fovChange(tf2_ros::Buffer & tfBuffer)
  {
        // * take 3D pose of vS_frame, in camera referential
        // * turn into 2D point
        // * if 2D point not on picture : 
        //      - Just send the closest pic from the object that is
        //  respecting the conditions
        // * take the sub picture according to those limits
        // * resize the picture according to cropped resolution (hVial - wVial)
        // * send the picture through ros topic
        if (!picTaken && allCvImg.size() > 0)
        {
            geometry_msgs::TransformStamped transformStamped;              
            try
            {

                transformStamped = tfBuffer.lookupTransform(cameraRef, trackedRef,
                    ros::Time(0));

                Eigen::Affine3d frameInCam = Eigen::Translation3d(transformStamped.transform.translation.x,
                                transformStamped.transform.translation.y,
                            transformStamped.transform.translation.z) *
                    Eigen::Quaterniond(transformStamped.transform.rotation.w,
                            transformStamped.transform.rotation.x,
                            transformStamped.transform.rotation.y,
                            transformStamped.transform.rotation.z);

                
                Eigen::Vector3d fp(transformStamped.transform.translation.x,
                                transformStamped.transform.translation.y,
                            transformStamped.transform.translation.z);
                setFocusValue(fp);  

                curDepth = fp[2];

                cv::Point2d oriPt;
                int hDim ;
                int wDim ;

                getPlaneLimitsAtDepth_PU(frameInCam,
                oriPt,
                hDim,
                wDim);

                // std::vector<cv::Point3d> lims3D;
                // lims3D.push_back(cv::Point3d(frameInCam(0,3)-halfWL, frameInCam(1,3)-halfWL, frameInCam(2,3)) );
                // lims3D.push_back(cv::Point3d(frameInCam(0,3)+halfWL, frameInCam(1,3)+halfWL, frameInCam(2,3)) );

                //std::cout << "frameInCam 3D : " << frameInCam.matrix().block<3,1>(0,3) << std::endl;
                // std::vector<cv::Point3d> lims3D;
                // lims3D.push_back(cv::Point3d(frameInCam(0,3), frameInCam(1,3), frameInCam(2,3)) );
                // std::vector<cv::Point2d> lims2D;

                // from3Dto2D(lims3D,lims2D);
                //std::cout << "frameInCam 2D [row x col] : " << lims2D[0].y << " ; " << lims2D[0].x << std::endl;

                //std::cout << "oriPt : " << oriPt.y << " ; " << oriPt.x << std::endl;



                //hDim = std::min(hDim, hVial);
                //wDim = std::min(wDim, wVial);


                // check if the values are into the FoV of the camera
                if (oriPt.y > height-hDim)
                {
                    oriPt.y = height-hDim;
                }
                else if (oriPt.y < 0)
                {
                    oriPt.y = 0;
                }
                if (oriPt.x > width-wDim)
                {
                    oriPt.x = width-wDim;
                }
                else if (oriPt.x < 0)
                {
                    oriPt.x = 0;
                }       

                oriPt.x = int(oriPt.x);
                oriPt.y = int(oriPt.y);

                oriPtFocus = oriPt;
                hDimFocus = hDim;
                wDimFocus = wDim;

            }

            catch (tf2::TransformException &ex) {
                //std::cout << &ex << std::endl;
                ;;
            }  




                // oriPtFocus.y = std::max( std::min(height-hDimFocus, oriPtFocus.y  ), 0);
                // oriPtFocus.x = std::max( std::min(width-wDimFocus, oriPtFocus.x  ), 0);

                // oriPtFocus.y = std::max( std::min(height-hDimFocus, int(oriPtFocus.y)  ), 0);
                // oriPtFocus.x = std::max( std::min(width-wDimFocus, int(oriPtFocus.x)  ), 0);                
                //oriPtFocus.x = std::max( std::min(width-wDimFocus, int(oriPtFocus.x-wDimFocus/2)  ), 0);


                // std::cout << "oriPtFocus after filt (row x col) : " << oriPtFocus.y << " ; " << oriPtFocus.x << std::endl;
                //std::cout << "hDimFocus : " << hDimFocus << " ; wDimFocus : " << wDimFocus  << std::endl;
                bool dimInLimits = (hDimFocus > 0 && hDimFocus < height && wDimFocus > 0 && wDimFocus < width);
                bool oriPtFocusInLimits = (oriPtFocus.x > 0 && oriPtFocus.x < height && oriPtFocus.y > 0 && oriPtFocus.y < width);

                if (dimInLimits && oriPtFocusInLimits)
                {


                    // select the ROI
                    // (x, y, w, h), (x, y) left corner coordinate, and w, h are width and height of the rectangle.
                    //cv::Rect roi(oriPtFocus.y, oriPtFocus.x, hDimFocus, wDimFocus);
                    cv::Rect roi(oriPtFocus.x, oriPtFocus.y, wDimFocus, hDimFocus);                
                    picTreated = true;
                    cv_bridge::CvImage cvImg(allCvImg[0]->header, allCvImg[0]->encoding, allCvImg[0]->image);
                    allCvImg.erase(allCvImg.begin());

                    cvImg.header.frame_id = cameraRef;
                    picTreated = false;

                    //std::cout << "size : " << cvImg.image.size() << std::endl;
                    //std::cout << "rows : " << cvImg.image.rows << std::endl;
                    // rows : 1080 = hDimFocus , cols : 1920 = wDimFocus
                    // https://docs.opencv.org/3.4/d9/d0c/group__calib3d.html#gafa5aa3f40a472bda956b4c27bec67438
                    // x : colonnes ; y : lignes

                    //cv::Mat selectedPart(cvImg.image,roi);
                    cv::Mat selectedPart = cvImg.image(cv::Range(oriPtFocus.y, oriPtFocus.y+hDimFocus), cv::Range(oriPtFocus.x, oriPtFocus.x +wDimFocus));


                    // resize the selectedPart
                    cv::Mat resized_up;

                    //std::cout << "hVial : " << hVial << " ; wVial : " << wVial  << std::endl;

                    cv::resize(selectedPart, resized_up, cv::Size(wVial,hVial), cv::INTER_LINEAR);

                    if (canPubImg)
                    {
                        // display circle at position of vial (red)
                        cv::circle( cvImg.image ,
                        cv::Point2d( oriPtFocus.x+ wDimFocus/2, oriPtFocus.y+ hDimFocus/2),
                        height/64,
                        cv::Scalar( 0, 0, 255 ),
                        cv::FILLED,
                        cv::LINE_8 );
                        
                        // display rectangle corresponding to FOV (blue)
                        cv::rectangle( cvImg.image,
                                cv::Point2d( oriPtFocus.x, oriPtFocus.y ),
                                cv::Point2d( oriPtFocus.x+ wDimFocus, oriPtFocus.y+ hDimFocus),
                                cv::Scalar( 255, 0,0 ),
                                height/128,
                                cv::LINE_8 );                    

                        // display circle at center of image (green)
                        cv::circle(cvImg.image,
                        cv::Point2d( width/2, height/2),
                        height/64,
                        cv::Scalar( 0, 255,0),
                        cv::FILLED,
                        cv::LINE_8 );

                        // display 3D position of center of image (green)
                        // and of center of FOV (red)
                        //, expressed in camera referential, as text
                        std::vector<cv::Point2d> lims2D;
                        lims2D.push_back(  cv::Point2d( width/2, height/2) );
                        lims2D.push_back( cv::Point2d( oriPtFocus.x+ wDimFocus/2, oriPtFocus.y+ hDimFocus/2) );                        
                        std::vector<cv::Point3d> backTo3D;
                        std::vector<double> allDepth;
                        allDepth.push_back(curDepth);
                        allDepth.push_back(curDepth);

                        pU.from2Dto3D(lims2D,allDepth,backTo3D);

                        char* x;
                        asprintf(&x, "(%3.3f, %3.3f, %3.3f)", 
                        backTo3D[0].x, backTo3D[0].y, backTo3D[0].z  );
                        std::string s_CI = x;

                        cv::putText(cvImg.image,
                            s_CI,
                            cv::Point(0,200),
                            cv::FONT_HERSHEY_DUPLEX,                            
                            4,
                            cv::Scalar(0,255,0),
                            4
                        );

                        char* x2;
                        asprintf(&x2, "(%3.3f, %3.3f, %3.3f)", 
                        backTo3D[1].x, backTo3D[1].y, backTo3D[1].z  );
                        std::string s_CF = x2;

                        cv::putText(cvImg.image,
                            s_CF,
                            cv::Point(0,height-200),
                            cv::FONT_HERSHEY_DUPLEX,                            
                            4,
                            cv::Scalar(0,0,255),
                            4
                        );
                        // publish picture

                        cvImg.header.stamp = ros::Time::now();
                        
                        image_pub_full_.publish(cvImg.toImageMsg());
                        canPubImg = false;
                    }


                    //cvImg.image = selectedPart;
                    double curTime = ros::Time::now().toSec(); 



                    if (useTimer && pU.isTimerActive && pU.timeLeft < 0)
                    {
                        // special mode : 
                        // 

                        if (!calledAfterTimerStop)
                        {
                            // call for stop
                            msg_srv_action_gestion::SetString srv;
                            srv.request.data = "Reboot";
                            if (stopManipIfTimeout.exists())
                            {
                                bool keepLooking = true;
                                int i = 0;
                                while (ros::ok() && keepLooking && i < 10 )
                                {

                                    keepLooking = !(stopManipIfTimeout.call(srv));
                                    if (!keepLooking)
                                    {
                                        calledAfterTimerStop = true;
                                        pU.timeDisplayed = "TIMEOUT";
                                    }
                                    i+=1;
                                }

                                ros::spinOnce();

                            }

                        }
                        // display blank picture
                        pU.blankPicture(resized_up);

                    }
                    else if (useTimer && !pU.isTimerActive && gotStopTrig)
                    {
                        pU.blankPicture(resized_up);
                        if (pU.timeDisplayed != "TIMEOUT")
                        {
                            pU.timeDisplayed = "ATTENTE DU PROCHAIN TEST";
                        }
                    }

                    if (pU.timeLeft > 0)
                    {
                        int val = int(pU.timeLeft);

                        if (val !=  lastTL)
                        {
                            std_msgs::Int32 msg;
                            lastTL = val;
                            msg.data = val;
                            pubTimeLeft.publish(msg);
                        }

                    }                    
                    
                    pU.updateTimer(resized_up,curTime);

                    cvImg.image = resized_up;
                    cvImg.header.stamp = ros::Time::now();
                    image_pub_.publish(cvImg.toImageMsg());
                    //std::cout << "----------" << std::endl;

                    msg_srv_action_gestion::Float64MultiArrayStamped msg;
                    std_msgs::MultiArrayDimension dim;

                    dim.label = "win_size_pix";
                    dim.size = 2;

                    msg.array.layout.dim.push_back(dim);


                    msg.array.data.push_back(wVial);
                    msg.array.data.push_back(hVial);
                    msg.header.stamp = ros::Time::now();                
                    winSizePub.publish(msg);

                    // reset
                    hDimFocus = -1;
                    wDimFocus = -1;
                }

        }
  }



    void fovManager::initialize(tf2_ros::Buffer & tfBuffer)
    {
        if (!camSetUp)
        {
            Eigen::Vector3d cameraPosition;
            geometry_msgs::TransformStamped transformStamped;
            geometry_msgs::TransformStamped transformStamped2;  
            geometry_msgs::TransformStamped transformStamped3;  
            bool gotPoses = false;
            std::string missTransfo;
            try
            {

                // initialize cameras positions
                missTransfo = globalRef + "->" + cubeCenter ;
                transformStamped = tfBuffer.lookupTransform(globalRef, cubeCenter,
                    ros::Time(0));
                missTransfo = globalRef + "->" + cameraRef;
                transformStamped2 = tfBuffer.lookupTransform(globalRef, cameraRef,
                                    ros::Time(0));   
                missTransfo = cameraRef + "->" + cubeCenter;                                    
                transformStamped3 = tfBuffer.lookupTransform(cameraRef, cubeCenter,
                    ros::Time(0));

                gotPoses = true;
            }
            catch (tf2::TransformException &ex) {
                // std::cout << ex << std::endl;
                //ROS_INFO("Do not have all initialization poses. Missing frames : %s", missTransfo.c_str());
                
                ;;
            }           

            if (gotPoses)        
            {           
                // 

                if (minDepth < 0)
                {
                    // rather use the distance between the center of the vial 
                    // and the center of the camera
                    minDepth = transformStamped3.transform.translation.z;

                }

                Eigen::Affine3d cartCenterInCam = Eigen::Translation3d(0.0,
                                0.0,
                            minDepth) *
                    Eigen::Quaterniond(1.0,
                            0.0,
                            0.0,
                            0.0);


                Eigen::Affine3d cartCenterInWorld = Eigen::Translation3d(transformStamped.transform.translation.x,
                                transformStamped.transform.translation.y,
                            transformStamped.transform.translation.z) *
                    Eigen::Quaterniond(transformStamped.transform.rotation.w,
                            transformStamped.transform.rotation.x,
                            transformStamped.transform.rotation.y,
                            transformStamped.transform.rotation.z);


                Eigen::Affine3d camInWorld = Eigen::Translation3d(transformStamped2.transform.translation.x,
                                transformStamped2.transform.translation.y,
                            transformStamped2.transform.translation.z) *
                    Eigen::Quaterniond(transformStamped2.transform.rotation.w,
                            transformStamped2.transform.rotation.x,
                            transformStamped2.transform.rotation.y,
                            transformStamped2.transform.rotation.z);

                //std::cout << "cam In World : " << camInWorld.matrix() << std::endl;

                cameraPosition(0,0) = transformStamped2.transform.translation.x;
                cameraPosition(1,0) = transformStamped2.transform.translation.y;
                cameraPosition(2,0) = transformStamped2.transform.translation.z;

                Eigen::Vector3d vDirInCam(transformStamped3.transform.translation.x,
                                transformStamped3.transform.translation.y,
                            transformStamped3.transform.translation.z);

                vDirInCam.normalize();

                int index = -1;
                double maxDist = 0.0;
                for (int i = 0; i <3; i++)
                {
                    if (abs(vDirInCam[i]) > maxDist)
                    {
                        index = i;
                        maxDist = abs(vDirInCam[i]);
                    }
                }
                double direction;

                if (vDirInCam[index] >0 )
                {
                    direction = 1.0;
                }
                else
                {
                    direction = -1.0;
                }

                //std::cout << "vDir : " << vDirInCam << std::endl;

                vDir_ind = index;

                //minDepth = transformStamped3.transform.translation.z - halfC;


                // get maximum size possible of the square, according to the given
                // minimal distance admissible between the tracked object and the camera.

                std::vector<double> allDepths = {minDepth, minDepth};
                cv::Point2d limTL, limBR;

                double distLim;

                if (height>width)
                {
                    // rows (or y) > cols (or x)
                    distLim = width/2;
                }
                else
                {
                    // cols (or x) > rows (or y) (most probable if no mistakes)
                    distLim = height/2;
                }

                limTL.x = width/2 - distLim;                
                limTL.y = height/2- distLim;

                limBR.x = width/2 + distLim;
                limBR.y = height/2 + distLim;

                std::vector<cv::Point2d> maxLimsAtDepth = {limTL,limBR};
                std::vector<cv::Point3d> limitsAtDepth;

                from2Dto3D(maxLimsAtDepth,allDepths,limitsAtDepth);

                double curMaxWindow_x = limitsAtDepth[1].x - limitsAtDepth[0].x;
                double curMaxWindow_y = limitsAtDepth[1].y - limitsAtDepth[0].y;

                double curMaxHalfWindow = std::min(curMaxWindow_y,curMaxWindow_x)/2;

                if (curMaxHalfWindow < halfWL)
                {
                    ROS_WARN("Warning : the maximum size of the window at depth %f is %f; however, %f was set as the window size. Reducing the window to size %f to solve this. \n", minDepth, 2*curMaxHalfWindow, 2*halfWL, 2*curMaxHalfWindow );
                    halfWL = curMaxHalfWindow;
                }


                // If we want to set as center of camera
                getPlaneLimitsAtDepth(cartCenterInCam,
                0.0,
                oriHWVial,
                hVial,
                wVial);                

                //std::cout << "===WORLD TEST=====" << std::endl;

                // int htst;
                // int wtst;
                // cv::Point2d oriTst;

                // getPlaneLimitsAtDepth_World(cartCenterInCam,
                // camInWorld,
                // -direction*halfC,
                // oriTst,
                // htst,
                // wtst);

                //std::cout << "Cart center in world : " << cartCenterInWorld.matrix() << std::endl;

                // std::cout << "oritst : " << oriTst.y << " ; " << oriTst.x << std::endl;
                // std::cout << "hTST : " << htst << " ; wTST : " << wtst  << std::endl;

                // std::cout << "oriHWVial : " << oriHWVial.y << " ; " << oriHWVial.x << std::endl;
                // std::cout << "hvial : " << hVial << " ; wvial : " << wVial  << std::endl;

                //hvial = std::max(hvial,wvial);
                // ko if 1280x720
                //bool allOk = abs( hVial/wVial - 1) < 0.1;
                //std::cout << hVial/wVial << std::endl;



                bool allOk = true;

                std::vector<int> vLims = { int(oriHWVial.y), int(oriHWVial.y+hVial), int(oriHWVial.x), int(oriHWVial.x+wVial) };
                int i = 0;
                while (i < vLims.size() && allOk)
                {
                    int max;
                    if (i<2)
                    {
                        max = height;
                    }
                    else
                    {
                        max = width;
                    }
                    if (vLims[i] < 0 || vLims[i] > max)
                    {
                        // could not happen : this would mean that the top left point or the bottom right points are not
                        // anymore on the screen (thus, ) 
                        std::cout << "Error at index : " << i << " : " << " value : " << vLims[i] << " ; max : " << max << std::endl;
                        allOk = false;

                    }
                    i++;

                }

                if (allOk)
                {
                    ROS_INFO("Maximum size of the window (cm , pix) : %3.4f , %d ", 2*halfWL, hVial);
                    std::cout << "dims : " << hVial << " , " << wVial << std::endl;

                    camSetUp = true;
                }
                else
                {
                    std::cout << "ERROR, FAILURE" << std::endl;
                    gotCamInfo = false;
                    pU.gotCamInfo = false;
                }
            }
        }

        if (!initialized && camSetUp)
        {
            
            switch (typeInput)
            {
                case (fovManagement::inputType::GST):
                {
                    initialized = (camCommand != "");
                }break;

                case (fovManagement::inputType::ROS):
                {
                    initialized = true;
                }break;                
            }
        }
       
                    
    }




    fovManager::fovManager(ros::NodeHandle & nh):
    nh_(nh),
    //cinfo_(new camera_info_manager::CameraInfoManager(nh))
    it_(nh)
    {   
        
        gotCamInfo = false;
        initialized = false;
        picTaken = false;
        picTreated = false;
        isCamInfoAsked = false;
        std::string nodeName = ros::this_node::getName();
        nodeName += "/";
        std::string controller_name;
        std::string input_type;

        bool gotParamTimer = rosWrap::getRosParam("/activate_timer", useTimer,1);

        if (!gotParamTimer)
        {
            // use other definition instead
            rosWrap::getRosParam(nodeName + "activate_timer", useTimer);
        }





        rosWrap::getRosParam(nodeName + "controller", controller_name);

        rosWrap::getRosParam(nodeName+"camera_sub",cameraSub);

        rosWrap::getRosParam(nodeName+"window_length",windowLength);

        rosWrap::getRosParam(nodeName+"camera_topic",cameraTopic);

        rosWrap::getRosParam(nodeName+"tracked_ref",trackedRef);

        rosWrap::getRosParam(nodeName+"input_type", input_type);
        double node_rate = 100;
        rosWrap::getRosParam(nodeName+"node_rate", node_rate,1);

        ros::Rate rate(node_rate);

        pubImg = rosWrap::getRosParam(nodeName+"fps_pub", fps_pub,1);
        pubImg = (fps_pub != -1 && pubImg);

        cameraTopic = cameraSub + "/" + cameraTopic;



        std::string varPath = "/panda/"+controller_name;
        /// .yaml path of different necessary variables
        rosWrap::getRosParam(nodeName+"pathToYAMLVars", varPath, 1);

        // 
        rosWrap::getRosParam(varPath+"/cube_center_tf", cubeCenter);

        rosWrap::getRosParam(varPath+"/global_ref", globalRef);

        rosWrap::getRosParam(varPath+"/camera_ref", cameraRef);
        // Minimum distance possible between the tracked object and
        // the center of the camera.
        // This distance will 
        rosWrap::getRosParam(varPath + "/min_in_depth", minDepth);



        if (input_type == "GST")
        {
            typeInput = fovManagement::inputType::GST;
        }

        else if (input_type == "ROS")
        {
            typeInput = fovManagement::inputType::ROS;
        }

        ROS_INFO("PUB TYPE : ");
        std::cout << input_type << std::endl;

        std::string camera_info_topic = cameraSub+"/camera_info";

        ros::Subscriber intrinsic = nh.subscribe(camera_info_topic, 1, &fovManager::cameraInfoSub, this);

        camInfoSender = nh.serviceClient<msg_srv_action_gestion::SetCamInfo>("/verified_cam_info",1);
        camInfoSender2 = nh.serviceClient<msg_srv_action_gestion::SetCamInfo>("/wp/verified_cam_info",1);


        getTimerTrig = nh.subscribe("/start_timer",1, &fovManager::manageTimerTrig, this);
        sTServer = nh.advertiseService("/stop_timer_fov",&fovManager::stopTrig,this);            
        // sTServer = nh.advertiseService("/stop_timer",&fovManager::stopTrig,this);            

        pCTServer = nh.advertiseService("/pause_continue_timer",&fovManager::pauseContinueTrig,this);    

        

        pubTimeLeft = nh.advertise<std_msgs::Int32>("/time_left_timer",1000);    


        // stopManipIfTimeout = nh.serviceClient<msg_srv_action_gestion::SetString>("/management_state");
   
        stopManipIfTimeout = nh.serviceClient<msg_srv_action_gestion::SetString>("/timeout_warning");
      

        if (typeInput == fovManagement::inputType::GST)
        {
            ROS_INFO("GST Service setup");
            clockServer = nh.advertiseService("/camera_management",&fovManager::cameraStringServer,this);
            fCServer = nh.advertiseService("/focus_calib_trigger",&fovManager::focusCalibTrigger,this);            
        }

        tf2_ros::Buffer tfBuffer;
        tf2_ros::TransformListener tfListener(tfBuffer);


        //halfC = cubeLength/2;
        halfWL = windowLength/2;


        while (ros::ok())
        {
            if (gotCamInfo)
            {
                if (!initialized)
                {

                    initialize(tfBuffer);
                    if (initialized)
                    {
                        intrinsic.shutdown();
                        ROS_INFO("FOV_MANAGER INITIALIZED");
                        if (typeInput == fovManagement::inputType::ROS)
                        {
                            image_sub_ = it_.subscribe(cameraTopic, 1000,
                            &fovManager::imageCb, this);
                        }

                        winSizePub = nh.advertise<msg_srv_action_gestion::Float64MultiArrayStamped>("/target_win_infos",100);

                        image_pub_ = it_.advertise("/pic_fov", 1000);
                        if (pubImg)
                        {
                            image_pub_full_ = it_.advertise("/pic_full", 1000);
                        }
                        ciServer = nh.advertiseService("/ask_cam_info",  &fovManager::ask_cam_info,this);
                        camParams = nh.serviceClient<msg_srv_action_gestion::SetStringWR>("/cam_param_request",1);

                    }

                }
                else
                {
                    fovChange(tfBuffer);
                }

                if (isCamInfoAsked && initialized)
                {
                    send_cam_info();
                }
            }
            // else{
            //     std::cout << "no cam info msg " << std::endl;
            // }
            rate.sleep();
            ros::spinOnce();
        }

        // get camera info (from https://github.com/ros-drivers/camera1394/blob/master/src/nodes/driver1394.cpp)
        //cinfo_->setCameraName(cameraSub);
        // sensor_msgs::CameraInfoPtr
        // ci(new sensor_msgs::CameraInfo(cinfo_->getCameraInfo()));

        // while (!cinfo_->isCalibrated() )
        // {
        //     ci = cinfo_->getCameraInfo();
        // }




        // image_sub_ = it_.subscribe("/kinect2/hd/image_color_rect", 1000,
        // &ImageConverter::imageCb, this);
        // image_pub_ = it_.advertise("/kinect2/hd/image_color_rect_balance", 1000);



        // get camera_info




        // https://www.tutorialkart.com/opencv/python/opencv-python-resize-image/

        // https://github.com/yukkysaito/autoware_thesis/blob/master/ros/src/sensing/fusion/packages/points2image/nodes/points2image/points2image.cpp


    }

};



/**
 * This tutorial demonstrates simple sending of messages over the ROS system.
 */
int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "fov_manager");
  ros::NodeHandle nh;

  fovManagement::fovManager fovM(nh);
  return 0;
}