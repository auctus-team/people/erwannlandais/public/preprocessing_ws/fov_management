#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Header.h"
#include "std_msgs/Empty.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float64.h"
#include <sstream>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "opencv2/core/core.hpp"
#include "opencv2/calib3d/calib3d.hpp"

#include <vector>

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>
#include <eigen3/Eigen/LU>

#include <tf2_ros/transform_listener.h>
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "sensor_msgs/CameraInfo.h"

#include <camera_info_manager/camera_info_manager.h>

#include <std_srvs/SetBool.h>
#include <msg_srv_action_gestion/SetCamInfo.h>
#include <msg_srv_action_gestion/SetString.h>
#include <msg_srv_action_gestion/SetStringWR.h>
#include <msg_srv_action_gestion/Float64MultiArrayStamped.h>
#include <msg_srv_action_gestion/GetFloat.h>


#include "stdio.h"

#include "ros_wrap/rosparam_getter.h"

#include "opencv_pinhole_management/pinhole_utils.h"

#include "std_msgs/Int32.h"

namespace fovManagement
{

enum inputType {GST,ROS};

class fovManager
{
    public:

std::vector<std::string> splitString(
        std::string str,
        std::string delimeter);

        fovManager(ros::NodeHandle & nh);
        void from3Dto2D(std::vector<cv::Point3d> all3Dpoints, std::vector<cv::Point2d> & all2Dpoints);
        void from2Dto3D(std::vector<cv::Point2d> all2Dpoints,
                                    std::vector<double>  allDepth,
                                    std::vector<cv::Point3d> & all3Dpoints);
        
        // Apply a supplementary transformation to points
        void from2Dto3D(std::vector<cv::Point2d> all2Dpoints,
                                    std::vector<double>  allDepth,
                                    std::vector<cv::Point3d> & all3Dpoints,
                                    Eigen::Affine3d camInWorld);

        
        float computeFocusValue(Eigen::Vector3d pos);

        void setFocusValue(Eigen::Vector3d pos);


        void cameraInfoSub(const sensor_msgs::CameraInfo & msg);

        bool cameraStringServer(msg_srv_action_gestion::SetStringWR::Request &req,
               msg_srv_action_gestion::SetStringWR::Response &res);


        void initialize(tf2_ros::Buffer & tfBuffer);
        void fovChange(tf2_ros::Buffer & tfBuffer);
        void imageCb(const sensor_msgs::ImageConstPtr& msg);

        void getPlaneLimitsAtDepth(Eigen::Affine3d centerPoseInCam, 
        double depth, 
        cv::Point2d & oriPt,
        int & hPlane,
        int & wPlane);

        void getPlaneLimitsAtDepth_PU(Eigen::Affine3d trackedPoseInCam,  
        cv::Point2d & oriPt,
        int & hPlane,
        int & wPlane);


    void getPlaneLimitsAtDepth_World(Eigen::Affine3d centerPoseInCam, Eigen::Affine3d camInWorld, 
    double depth, 
    cv::Point2d & oriPt,
    int & hPlane,
    int & wPlane);

       void from3Dto2D(std::vector<cv::Point3d> all3Dpoints, cv::Mat Ts, cv::Mat rvecRs, std::vector<cv::Point2d> & all2Dpoints);
       
       bool ask_cam_info(std_srvs::SetBool::Request &req,
            std_srvs::SetBool::Response &res);

            void send_cam_info();

       bool focusCalibTrigger(std_srvs::SetBool::Request &req,
            std_srvs::SetBool::Response &res);

    bool stopTrig(msg_srv_action_gestion::GetFloat::Request &req,
    msg_srv_action_gestion::GetFloat::Response &res);


        void manageTimerTrig(const std_msgs::Float64 & msg);

    bool pauseContinueTrig(std_srvs::SetBool::Request &req,
    std_srvs::SetBool::Response &res);

        ros::NodeHandle nh_;
        image_transport::ImageTransport it_;
        //camera_info_manager::CameraInfoManager cinfo_;
        image_transport::Subscriber image_sub_;
        image_transport::Publisher image_pub_;
        image_transport::Publisher image_pub_full_;        
        cv_bridge::CvImagePtr cv_ptr;

        bool useTimer = false;

        bool gotCamInfo;
        bool initialized;
        bool camSetUp = false;
        //boost::shared_ptr<camera_info_manager::CameraInfoManager> cinfo_;


        std::string cameraSub;
        std::string cameraTopic;
        std::string cubeCenter;
        double cubeLength;
        std::string globalRef;    
        std::string cameraRef;
        std::string trackedRef;

        // std::vector<cv::Point3d> limPts3D;
        // std::vector<cv::Point2d> limPts2D;
        double halfC;
        double halfWL;


        int height;
        int width;

        int hVial;
        int wVial;
        cv::Point2d oriHWVial;
        int vDir_ind;
        // not usefull at all, will need to be suppressed
        bool picTaken;
        bool picTreated;

        ros::Publisher pubTimeLeft;

        bool gotStopTrig = true;
        int lastTL = -1;

        // maximal length of a side of the window
        // (in meter)
        double windowLength;

        double minDepth;

        cv::Mat distCoeffs;    
        cv::Mat camMat;            
        cv::Mat rvecR;         
        cv::Mat T;
        cv::Mat K;


        std::vector<cv_bridge::CvImagePtr> allCvImg;

        cv::Mat curPic;

        bool isCamInfoAsked;

        ros::ServiceClient camInfoSender;
        ros::ServiceClient camInfoSender2;

        inputType typeInput;
        ros::ServiceServer clockServer;

        ros::ServiceServer ciServer;
        ros::ServiceServer sTServer;
        ros::ServiceServer pCTServer;


        ros::ServiceClient stopManipIfTimeout;
        ros::ServiceClient camParams;

        ros::Subscriber getTimerTrig;


        std::string camCommand = "";
        std::string encodingType;

        boost::shared_ptr<cv::VideoCapture> cap;       

        ros::Publisher winSizePub; 

        int fps_pub;
        bool pubImg;
        
        bool canPubImg = false;

        int cntPubImg = 0;

        pinholeUtils pU;

        bool focusCorrection = true;
        ros::ServiceServer fCServer;     

        bool calledAfterTimerStop = false;   

        Eigen::Affine3d curfIC;
        cv::Point2d oriPtFocus;
        int hDimFocus = -1;
        int wDimFocus = -1;
        double curDepth = -1;


        

};

}