#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Header.h"
#include "std_msgs/Empty.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float64.h"
#include <sstream>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "opencv2/core/core.hpp"
#include "opencv2/calib3d/calib3d.hpp"

#include <vector>

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>
#include <eigen3/Eigen/LU>

#include <tf2_ros/transform_listener.h>
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "sensor_msgs/CameraInfo.h"


#include <std_srvs/SetBool.h>
#include <msg_srv_action_gestion/SetCamInfo.h>
#include <msg_srv_action_gestion/SetString.h>
#include <msg_srv_action_gestion/SetStringWR.h>
#include <msg_srv_action_gestion/Float64MultiArrayStamped.h>
#include <msg_srv_action_gestion/GetFloat.h>


#include "stdio.h"

#include "ros_wrap/rosparam_getter.h"

#include "opencv_pinhole_management/pinhole_utils.h"

#include "tf_management_pkg/tf_utils.h"

#include "std_msgs/Int32.h"

namespace camToView
{

enum inputType {GST,ROS};

class trackPoint
{
    public:


        trackPoint(ros::NodeHandle & nh);

        int computeFocusValue(Eigen::Vector3d pos);

        void setFocusValue(Eigen::Vector3d pos);

        void cameraInfoSub(const sensor_msgs::CameraInfo & msg);


        void initialize();
        void addInfosOnView();
        void imageCb(const sensor_msgs::ImageConstPtr& msg);


        ros::NodeHandle nh_;
        image_transport::ImageTransport it_;

        image_transport::Subscriber image_sub_;
        image_transport::Publisher image_pub_;
        image_transport::Publisher image_pub_full_;        
        cv_bridge::CvImagePtr cv_ptr;

        bool gotCamInfo;
        bool initialized;
        bool camSetUp = false;


        std::string cameraSub;
        std::string inputCameraTopic;
        std::string outputCameraTopic;

        std::string globalRef;    
        std::string cameraRef;
        std::string trackedRef;


        int height;
        int width;

        // not usefull at all, will need to be suppressed
        bool picTaken;
        bool picTreated;

        std::vector<cv_bridge::CvImagePtr> allCvImg;

        inputType typeInput;
        
        std::string camCommand = "";
        std::string encodingType;

        boost::shared_ptr<cv::VideoCapture> cap;       

        pinholeUtils pU;
        tfUtils tU;

        bool focusCorrection = true;
      
        std::vector<double> focusDistance; // = {0.07,0.08,0.09,0.10,0.11,0.12,0.13,0.18,0.21,0.29,0.45,0.60,99999};
        std::vector<int> focusValue; // = {115,110,105,90,85,80,75,70,65,60,55,50,45};

        std::string camID;

};

}