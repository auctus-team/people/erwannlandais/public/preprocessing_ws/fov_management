# fov_management

The goal of this package is to get a constant square cropped part of a video stream (ex : 6 cm square around a point, whather the XYZ position of this point from the center of the camera).
This cropped part can have an autofocus on this part. 
It can also display the full video stream with the position of the cropped part as a blue square. 

The input video stream can be obtained through with ROS or Gstreamer.
For ROS, you can get the video stream from your camera using http://wiki.ros.org/video_stream_opencv . 
For Gstreamer, you will need the gstreamer_to_ros_utils package.

The output of this package will be a ROS topic, containing the modified video stream.

Necessary packages (from ros_management_ws) : 
- msg_srv_action_gestion
- ros_wrap
- opencv_pinhole_management

## Specific note for teleoperation platform

To desactivate the timer use, please put activate_timer at false on fov_manager.launch. 